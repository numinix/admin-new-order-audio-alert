<?php
$configuration = $db->Execute("SELECT configuration_group_id FROM " . TABLE_CONFIGURATION_GROUP . " WHERE configuration_group_title = 'Admin New Order Audio Alert' ORDER BY configuration_group_id ASC;");
if ($configuration->RecordCount() > 0) {
  while (!$configuration->EOF) {
    $db->Execute("DELETE FROM " . TABLE_CONFIGURATION . " WHERE configuration_group_id = " . $configuration->fields['configuration_group_id'] . ";");
    $db->Execute("DELETE FROM " . TABLE_CONFIGURATION_GROUP . " WHERE configuration_group_id = " . $configuration->fields['configuration_group_id'] . ";");
    $configuration->MoveNext();
  }
}

$db->Execute("INSERT INTO " . TABLE_CONFIGURATION_GROUP . " (configuration_group_title, configuration_group_description, sort_order, visible) VALUES ('Admin New Order Audio Alert', 'Admin New Order Audio Alert Settings', '1', '1');");
$configuration_group_id = $db->Insert_ID();

$db->Execute("UPDATE " . TABLE_CONFIGURATION_GROUP . " SET sort_order = " . $configuration_group_id . " WHERE configuration_group_id = " . $configuration_group_id . ";");

$db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES
            ('Version', 'ADMIN_AUDIO_ALERT_VERSION', '1.0.0', 'Version installed:', " . $configuration_group_id . ", 1, NOW(), NOW(), NULL, NULL), 
            ('Sound File', 'ADMIN_AUDIO_ALERT_FILE', 'cha_ching.mp3', 'The file that should play when a new order is placed', " . $configuration_group_id . ", 10, NOW(), NOW(), NULL, NULL),    
            ('Module Enabled', 'ADMIN_AUDIO_ALERT_ACTIVE', 'true', 'Control if this module is used. <br/> true = On <br/> false = Off', " . $configuration_group_id . ", 5, NOW(), NOW(), NULL,'zen_cfg_select_option(array(\"true\", \"false\"),'),
            ('Frequncy Rate', 'ADMIN_AUDIO_ALERT_FREQUENCY', '30', 'The number of secounds between each time checking for a new order. (lower means it is checked more often, but lower means more work on the server too.)', " . $configuration_group_id . ", 15, NOW(), NOW(), NULL, NULL);");

$zc150 = (PROJECT_VERSION_MAJOR > 1 || (PROJECT_VERSION_MAJOR == 1 && substr(PROJECT_VERSION_MINOR, 0, 3) >= 5));
if ($zc150) { // continue Zen Cart 1.5.0
  // delete configuration menu
  $db->Execute("DELETE FROM ".TABLE_ADMIN_PAGES." WHERE page_key = 'configAdminAudioAlert' LIMIT 1;");
  // add configuration menu
  if (!zen_page_key_exists('configAdminAudioAlert')) {
    $configuration = $db->Execute("SELECT configuration_group_id FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'ADMIN_AUDIO_ALERT_VERSION' LIMIT 1;");
    $configuration_group_id = $configuration->fields['configuration_group_id'];
    if ((int)$configuration_group_id > 0) {
      zen_register_admin_page('configAdminAudioAlert',
                              'BOX_CONFIG_ADMIN_AUDIO_ALERT', 
                              'FILENAME_CONFIGURATION',
                              'gID=' . $configuration_group_id, 
                              'configuration', 
                              'Y',
                              $configuration_group_id);
        
      $messageStack->add('Enabled Admin New Order Audio Alert Configuration Menu.', 'success');
    }
  }
}
