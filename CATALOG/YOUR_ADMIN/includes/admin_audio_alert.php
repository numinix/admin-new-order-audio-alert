<?php

//check if superuser
$superuser = $db->Execute('SELECT admin_profile FROM ' . TABLE_ADMIN . ' WHERE admin_id = ' . $_SESSION['admin_id']);

if($superuser->fields['admin_profile'] == 1){
	if(ADMIN_AUDIO_ALERT_ACTIVE == "true"){ ?>
	<iframe name="adminAudioAlert" id="adminAudioAlert" frameborder="0" width="100px" height="1px" src="admin_audio_alert_iframe.php"></iframe>
	<?php }
} else {
	$admin_check = $db->Execute('SELECT aptp.page_key FROM ' . TABLE_ADMIN . ' a LEFT JOIN ' . TABLE_ADMIN_PAGES_TO_PROFILES . ' aptp on a.admin_profile=aptp.profile_id WHERE a.admin_id = ' . $_SESSION['admin_id']);

	while(!$admin_check->EOF){
		if($admin_check->fields['page_key'] == 'configAdminAudioAlert'){
			if(ADMIN_AUDIO_ALERT_ACTIVE == "true"){ ?>
			<iframe name="adminAudioAlert" id="adminAudioAlert" frameborder="0" width="100px" height="1px" src="admin_audio_alert_iframe.php"></iframe>
			<?php
			}
			break;
		}
		$admin_check->MoveNext();
	}
}